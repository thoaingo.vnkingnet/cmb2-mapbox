<?php
/**
 * @package CMB2_Field_Mapbox
 */
/*
Plugin Name: CMB2_Field_Mapbox
Plugin URI: https://duongtambeautyspa.com/
Description: CMB2_Field_Mapbox this a plugin for Wordpress. It is used integration with cmb2 plugin. You only add token mapbox, then you can use it. It is develop by NgoThoai (ChengChivas). It is support show map of using mapbox.
Version: 1.0.0
Author: ChengChivas
Author URI: https://duongtambeautyspa.com/
License: GPLv2 or later
Text Domain: chengchivas
*/
class CMB2_Field_Mapbox {

	/**
	 * Current version number.
	 */
	const VERSION = '1.0.0';
	define(TOCKEN, '');
	define(ICONMAP, 'https://i.imgur.com/MK4NUzI.png');


	/**
	 * Initialize the plugin by hooking into CMB2.
	 */
	public function __construct() {
		add_filter( 'cmb2_render_mapbox', array( $this, 'render_mapbox' ), 10, 5 );
		add_filter( 'cmb2_sanitize_mapbox', array( $this, 'sanitize_mapbox' ), 10, 4 );

	}

	/**
	 * Render field.
	 */
	public function render_mapbox( $field, $field_escaped_value, $field_object_id, $field_object_type, $field_type_object ) {

		$this->setup_admin_scripts();
		echo '<div class="mapbox-form">';
		echo '<div class="mapbox-render" id="mapbox-render-search"></div>';
		echo '<div id="geocoder" class="geocoder"></div>';
		echo '</div>';
		$field_type_object->_desc( true, true );

		echo $field_type_object->input( array(
			'type'       => 'hidden',
			'name'       => $field->args('_name') . '[latitude]',
			'value'      => isset( $field_escaped_value['latitude'] ) ? $field_escaped_value['latitude'] : '',
			'class'      => 'cmb2-map-latitude',
			'id'      => $field->args('_name') . '[latitude]',
			'desc'       => '',
		) );
		echo $field_type_object->input( array(
			'type'       => 'hidden',
			'name'       => $field->args('_name') . '[longitude]',
			'value'      => isset( $field_escaped_value['longitude'] ) ? $field_escaped_value['longitude'] : '',
			'class'      => 'cmb2-map-longitude',
			'id'      => $field->args('_name') . '[longitude]',
			'desc'       => '',
		) );
		echo $field_type_object->input( array(
			'type'       => 'text',
			'name'       => $field->args('_name') . '[zoom]',
			'value'      => isset( $field_escaped_value['zoom'] ) ? $field_escaped_value['zoom'] : '13',
			'class'      => 'cmb2-map-zoom',
			'id'      => $field->args('_name') . '[zoom]',
			'label'       => esc_html__('Zoom', 'chengchivas'),
			'desc'       => '',
			'placeholder' => esc_html__('Zoom map', 'chengchivas')
		) );
	}

	/**
	 * Optionally save the latitude/longitude values into two custom fields.
	 */
	public function sanitize_mapbox( $override_value, $value, $object_id, $field_args ) {
		if ( isset( $field_args['split_values'] ) && $field_args['split_values'] ) {
			if ( ! empty( $value['latitude'] ) ) {
				update_post_meta( $object_id, $field_args['id'] . '_latitude', $value['latitude'] );
			}

			if ( ! empty( $value['longitude'] ) ) {
				update_post_meta( $object_id, $field_args['id'] . '_longitude', $value['longitude'] );
			}
		}

		return $value;
	}

	/**
	 * Enqueue scripts and styles.
	 */
	public function setup_admin_scripts() {
		wp_enqueue_style( 'mapbox-gl.css', 'https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.css');
        wp_enqueue_script( 'mapbox-gl.js', 'https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.js');
		wp_enqueue_style( 'mapbox-geocoder.css', 'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css');
        wp_enqueue_script( 'mapbox-geocoder.js', 'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js');
        $theme_option = get_option('theme_option');
        $access_token_mapbox = isset(TOCKEN) ? TOCKEN : "";
        $icon_map = isset(ICONMAP) ? ICONMAP : "";
        $dataToBePassed = array();
        $dataToBePassed = array(
            'access_token_mapbox'            => $access_token_mapbox,
            'icon_map'            => $icon_map,
        );
        wp_enqueue_script( 'cmb2-mapbox', plugins_url( 'js/script.js', __FILE__ ), array( 'mapbox-gl.js','mapbox-geocoder.js', 'jquery' ), self::VERSION );
        wp_localize_script( 'cmb2-mapbox', 'object_jquery_admin',$dataToBePassed);



		
		wp_enqueue_style( 'cmb2-mapbox-css', plugins_url( 'css/style.css', __FILE__ ), array(), self::VERSION );
	}

	/**
	 * Default filter to return a Google API key constant if defined.
	 */
}
$cmb2_field_Mapbox = new CMB2_Field_Mapbox();
