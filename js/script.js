mapboxgl.accessToken = object_jquery_admin.access_token_mapbox;

(function( $ ) {
	'use strict';
	var mapbox_input_lat=$('.cmb-type-cmb2-mapbox').find('.cmb2-map-latitude');
	var mapbox_input_zoom=$('.cmb-type-cmb2-mapbox').find('.cmb2-map-zoom').val();
	var mapbox_input_lat_val=mapbox_input_lat.val();
    var mapbox_input_lng=$('.cmb-type-cmb2-mapbox').find('.cmb2-map-longitude');
    var mapbox_input_lng_val=mapbox_input_lng.val();
    if(typeof mapbox_input_lat_val === 'underfind' || typeof mapbox_input_lng_val === 'underfind' || mapbox_input_lat_val === ''  || mapbox_input_lng_val === ''){
    	if(navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(position => {
		    	var mapbox_input_lat_val=position.coords.latitude;
		    	var mapbox_input_lng_val=position.coords.longitude;
		    	mapbox_input_lat.val(mapbox_input_lat_val);
        		mapbox_input_lng.val(mapbox_input_lng_val);
		    });
		} else {
		}
    } else {
    	var mapbox_input_lat_val=mapbox_input_lat.val();
    	var mapbox_input_lng_val=mapbox_input_lng.val();
    }
    if(typeof mapbox_input_zoom === 'undefined'){
    	var mapbox_input_zoom = 13;
    }
     var icon_map = object_jquery_admin.icon_map;
     console.log(icon_map);
    if(typeof icon_map !== 'underfind' ){
        var icon_map = icon_map;
    } else {
        var icon_map = "https://i.imgur.com/MK4NUzI.png";
    }
	var map = new mapboxgl.Map({
		container: 'mapbox-render-search',
		style: 'mapbox://styles/mapbox/streets-v11?optimize=true',
		center: [mapbox_input_lng_val, mapbox_input_lat_val],
		zoom: mapbox_input_zoom
	});
	 map.on("load", function() {
        map.resize();
        map.loadImage(icon_map, function(error, image) {
            if (error) throw error;
            map.addImage("custom-marker", image);
            map.addLayer({
                id: "points",
                type: "symbol",
                source: {
                    type: "geojson",
                    data: {
                        type: 'FeatureCollection',
                        features: [{
                            type: 'Feature',
                            properties: {},
                            geometry: {
                                type: "Point",
                                coordinates: [mapbox_input_lng_val, mapbox_input_lat_val]
                            }
                        }]
                    }
                },
                layout: {
                    "icon-image": "custom-marker",
                }
            });
        });
    });
	
	 var geocoder = new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl
    });

    document.getElementById('geocoder').appendChild(geocoder.onAdd(map));
    geocoder.on('result', function (result) {
    	
        var new_long = parseFloat(result.result.center[0]);
        var  new_lat = parseFloat(result.result.center[1]);
        mapbox_input_lat.val(new_lat);
        mapbox_input_lng.val(new_long);
    });
})( jQuery );